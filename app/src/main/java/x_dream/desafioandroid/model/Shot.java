package x_dream.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by X-Dream on 28/02/15.
 * Não é bom o uso de get e set no Android
 * http://developer.android.com/training/articles/perf-tips.html#GettersSetters
 */

@Parcel
public class Shot {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("height")
    public int height;

    @SerializedName("width")
    public int width;

    @SerializedName("likes_count")
    public int likesCount;

    @SerializedName("comments_count")
    public int commentsCount;

    @SerializedName("rebounds_count")
    public String reboundsCount;

    @SerializedName("url")
    public String url;

    @SerializedName("short_url")
    public String shortUrl;

    @SerializedName("views_count")
    public String viewsCount;

    @SerializedName("rebound_source_id")
    public String reboundSourceId;

    @SerializedName("image_url")
    public String imageUrl;

    @SerializedName("image_teaser_url")
    public String imageTeaserUrl;

    @SerializedName("image_400_url")
    public String image400Url;

    @SerializedName("player")
    public Player player;

    @SerializedName("created_at")
    public Date createdAt;

    public Shot(){
    }

}

