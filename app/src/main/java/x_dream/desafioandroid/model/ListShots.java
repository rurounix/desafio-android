package x_dream.desafioandroid.model;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by X-Dream on 01/03/15.
 * * Não é bom o uso de get e set no Android
 * http://developer.android.com/training/articles/perf-tips.html#GettersSetters
 */
@Parcel
public class ListShots {

    public int page;

    public int perPage;

    public int pages;

    public int total;

    public List<Shot> shots;

}
