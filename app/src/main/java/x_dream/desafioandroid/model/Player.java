package x_dream.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by X-Dream on 01/03/15.
 * * Não é bom o uso de get e set no Android
 * http://developer.android.com/training/articles/perf-tips.html#GettersSetters
 */
@Parcel
public class Player {

    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;

    @SerializedName("location")
    public String location;

    @SerializedName("followers_count")
    public int followersCount;

    @SerializedName("draftees_count")
    public int drafteesCount;

    @SerializedName("likes_count")
    public int likesCount;

    @SerializedName("likes_received_count")
    public int likesReceivedCount;

    @SerializedName("comments_count")
    public int commentsCount;

    @SerializedName("rebounds_count")
    public int reboundsCount;

    @SerializedName("rebounds_received_count")
    public int reboundsReceivedCount;

    @SerializedName("url")
    public String url;

    @SerializedName("avatar_url")
    public String avatarUrl;

    @SerializedName("username")
    public String userName;

    @SerializedName("twitter_screen_name")
    public String twitterScreenName;

    @SerializedName("website_url")
    public String webSiteUrl;

    @SerializedName("drafted_by_player_id")
    public int draftedPlayerId;

    @SerializedName("following_count")
    public int followeingCount;

    @SerializedName("created_at")
    public Date createdAt;
}
