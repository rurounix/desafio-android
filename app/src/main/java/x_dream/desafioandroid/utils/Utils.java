package x_dream.desafioandroid.utils;

import android.content.Context;
import android.content.Intent;

/**
 * Created by X-Dream on 01/03/15.
 */
public class Utils {

    public static final String PARAM_SHOT_ID = "paramShotId";


    public static void startShareIntent(String url, Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
}
