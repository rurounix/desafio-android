package x_dream.desafioandroid.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import retrofit.Callback;
import retrofit.RetrofitError;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import retrofit.client.Response;
import x_dream.desafioandroid.customListener.EndlessScrollListener;
import x_dream.desafioandroid.R;
import x_dream.desafioandroid.http.HttpRequest;
import x_dream.desafioandroid.model.ListShots;
import x_dream.desafioandroid.adapter.ShotAdapter;
import x_dream.desafioandroid.utils.Utils;


public class HomeActivity extends BaseActivity {

    private ListView mListShot;
    private OnItemClickListener mShotItemClick;
    private Context mContext;
    private OnRefreshListener mOnRefreshList;
    private SwipeRefreshLayout mSwipeLayout;
    private int mPage;
    private ShotAdapter mAdapter;
    private EndlessScrollListener mEndlessScrollListener;

    //controle da posição da listView
    private int index;
    private int top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.mContext = this;
        mPage = 1;
        loadListeners();
        loadViews();
        request();


    }

    private void loadListeners(){

        mShotItemClick = new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(mContext, ShotDetailsActivity.class);
                intent.putExtra(Utils.PARAM_SHOT_ID, mAdapter.getItem(position).id);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

            }
        };

        mOnRefreshList = new OnRefreshListener() {
            @Override
            public void onRefresh() {
            index = 0;
            request();

            }
        };

        mEndlessScrollListener = new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                index = mListShot.getLastVisiblePosition()+1;
                View v = mListShot.getChildAt(0);
                top = (v == null) ? 0 : (v.getTop() - mListShot.getPaddingTop());
                mPage = mPage+1;
                request(mPage);
            }
        };

    }

    private void loadViews(){

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        mSwipeLayout.setOnRefreshListener(mOnRefreshList);

        mListShot = (ListView) findViewById(R.id.list_shots);
        mAdapter = new ShotAdapter(mContext);
        mListShot.setOnItemClickListener(mShotItemClick);
        mListShot.setOnScrollListener(mEndlessScrollListener);

    }

    private void request(){
        request(1);
    }

    private void request(int page){
        mSwipeLayout.setRefreshing(true);

        HttpRequest.doRequest(this).listShot(page, new Callback<ListShots>() {
            @Override
            public void success(ListShots shotVOs, Response response) {

                    mAdapter.addAll(shotVOs.shots);

                mListShot.setAdapter(mAdapter);

                mListShot.setSelection(index);
                mSwipeLayout.setRefreshing(false);

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                mSwipeLayout.setRefreshing(false);
            }
        });
    }



}
