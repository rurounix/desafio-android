package x_dream.desafioandroid.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import x_dream.desafioandroid.R;
import x_dream.desafioandroid.http.HttpRequest;
import x_dream.desafioandroid.model.Shot;
import x_dream.desafioandroid.utils.Utils;

public class ShotDetailsActivity extends BaseActivity {

    private TextView mTvName;
    private TextView mTvViewCount;
    private ImageView mImageShot;
    private ProgressBar mProgressLoad;
    private Context mContext;
    private View mLayoutDetails;
    private ImageView mImgProfile;
    private TextView mTvProfileName;
    private TextView mTvDescription;
    private OnClickListener mOnClickShare;
    private Shot mShot;
    private ImageView mImageShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);
        mContext =this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int shotId = getIntent().getExtras().getInt(Utils.PARAM_SHOT_ID);

        loadListeners();
        loadView();

        HttpRequest.doRequest(this).shotDetails(shotId, new Callback<Shot>() {
            @Override
            public void success(Shot shot, Response response) {
                mProgressLoad.setVisibility(View.GONE);
                 mLayoutDetails.setVisibility(View.VISIBLE);
                mShot = shot;
                fillView(mShot);

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(mContext, error.getMessage(),Toast.LENGTH_SHORT).show();
                mProgressLoad.setVisibility(View.GONE);
            }
        });

    }
    private void loadView(){

        View mIncludedView = findViewById(R.id.layout_header);
        mProgressLoad = (ProgressBar) findViewById(R.id.progressBar);
        mImageShot = (ImageView) mIncludedView.findViewById(R.id.img_shout);
        mImageShare = (ImageView) mIncludedView.findViewById(R.id.img_share);
        mTvName = (TextView) mIncludedView.findViewById(R.id.tv_name);
        ImageView mImgShare = (ImageView) mIncludedView.findViewById(R.id.img_share);
        mTvViewCount = (TextView) mIncludedView.findViewById(R.id.tv_view_count);
        mLayoutDetails = findViewById(R.id.layout_details);
        mImgProfile = (ImageView) findViewById(R.id.img_profile);
        mTvProfileName = (TextView) findViewById(R.id.tv_profile_name);
        mTvDescription = (TextView) findViewById(R.id.tv_description);

        mImageShare.setOnClickListener(mOnClickShare);

    }

    private void loadListeners(){
       mOnClickShare = new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.startShareIntent(mShot.url, mContext);
            }
        };
    }

    private void fillView(Shot shot){

        Picasso.with(mContext).load(shot.imageTeaserUrl).placeholder(android.R.drawable.ic_menu_gallery).error(android.R.drawable.ic_dialog_alert).into(mImageShot);
        Picasso.with(mContext).load(shot.player.avatarUrl).placeholder(android.R.drawable.ic_menu_gallery).error(android.R.drawable.ic_dialog_alert).into(mImgProfile);
        mTvName.setText(shot.title);
        mTvViewCount.setText(String.valueOf(shot.viewsCount));
        mTvProfileName.setText(shot.player.name);
        if(shot.description !=null) {
            mTvDescription.setText(Html.fromHtml(shot.description));
        }else{
            mTvDescription.setVisibility(View.GONE);
        }
    }

}
