package x_dream.desafioandroid.http;

import android.content.Context;

import retrofit.RestAdapter;
import x_dream.desafioandroid.R;

/**
 * Created by X-Dream on 01/03/15.
 */
public class HttpRequest {



    public static HttpInterface doRequest(Context mContext){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(mContext.getString(R.string.SERVER_URL))
                .build();
       return restAdapter.create(HttpInterface.class);
    }



}
