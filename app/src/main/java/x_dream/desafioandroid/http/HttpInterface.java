package x_dream.desafioandroid.http;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import x_dream.desafioandroid.model.ListShots;
import x_dream.desafioandroid.model.Shot;



/**
 * Created by X-Dream on 01/03/15.
 */
public interface HttpInterface {

    @GET("/shots/popular")
    void listShot(@Query("page") int page, Callback<ListShots> callback);


    @GET("/shots/{id}")
    void shotDetails(@Path("id") int shotId, Callback<Shot> callback);

}