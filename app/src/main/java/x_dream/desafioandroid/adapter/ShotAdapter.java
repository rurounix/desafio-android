package x_dream.desafioandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import x_dream.desafioandroid.R;
import x_dream.desafioandroid.model.Shot;
import x_dream.desafioandroid.utils.Utils;

/**
 * Created by X-Dream on 28/02/15.
 */
public class ShotAdapter extends ArrayAdapter<Shot> {
    private final LayoutInflater inflater;
    private final Context mContext;

    public ShotAdapter(Context context) {
        super(context, R.layout.adapter_shot);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemLayout = convertView;
        ShotViewHolder holder;
        final Shot shot = this.getItem(position);
        if (convertView == null) {
            itemLayout = inflater.inflate(R.layout.adapter_shot, parent, false);
            holder = new ShotViewHolder();

            holder.imgShout = (ImageView) itemLayout.findViewById(R.id.img_shout);
            holder.tvName = (TextView) itemLayout.findViewById(R.id.tv_name);
            holder.tvViewCount = (TextView) itemLayout.findViewById(R.id.tv_view_count);
            holder.imgShare = (ImageView) itemLayout.findViewById(R.id.img_share);
            itemLayout.setTag(holder);
        }


        holder = (ShotViewHolder) itemLayout.getTag();
        holder.tvName.setText(shot.title);
        holder.tvViewCount.setText(String.valueOf(shot.viewsCount));
        holder.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.startShareIntent(shot.url, mContext);
            }
        });
        Picasso.with(mContext).load(shot.imageTeaserUrl).placeholder(android.R.drawable.ic_menu_gallery).error(android.R.drawable.ic_dialog_alert).into(holder.imgShout);

        return itemLayout;
    }


    private class ShotViewHolder {
        ImageView imgShout;
        ImageView imgShare;
        TextView tvName;
        TextView tvViewCount;
    }

}
