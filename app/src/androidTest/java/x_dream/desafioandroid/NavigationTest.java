package x_dream.desafioandroid;

import android.test.ActivityInstrumentationTestCase2;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;

import com.robotium.solo.Solo;

import junit.framework.Assert;

import x_dream.desafioandroid.activity.AboutActivity;
import x_dream.desafioandroid.activity.HomeActivity;
import x_dream.desafioandroid.activity.ShotDetailsActivity;

/**
 * Created by X-Dream on 01/03/15.
 */
public class NavigationTest extends ActivityInstrumentationTestCase2<HomeActivity> {

    private Solo solo;

    public NavigationTest() {
        super(HomeActivity.class);
    }

    public void setUp() throws Exception{
        solo = new Solo(getInstrumentation(), getActivity());
    }


    public void testNavigationToDetails() throws Exception{

        solo.assertCurrentActivity("Acitivity errada", HomeActivity.class);
        solo.clickInList(1);
        solo.waitForActivity(ShotDetailsActivity.class);
        solo.assertCurrentActivity("Acitivity errada", ShotDetailsActivity.class);
        solo.waitForView(R.id.img_profile);
        Assert.assertTrue(solo.getView(R.id.img_profile).isShown());

    }

    public void testNavigationToDetailsWithSwipe() throws Exception{

        solo.assertCurrentActivity("Acitivity errada", HomeActivity.class);
        solo.scrollDown();
        solo.clickInList(3);
        solo.waitForActivity(ShotDetailsActivity.class);
        solo.assertCurrentActivity("Acitivity errada", ShotDetailsActivity.class);
        solo.waitForView(R.id.img_profile);
        Assert.assertTrue(solo.getView(R.id.img_profile).isShown());

    }


    public void testNavigationToAbout() throws Exception{

        solo.assertCurrentActivity("Acitivity errada", HomeActivity.class);
        solo.sendKey(Solo.MENU);
        solo.clickOnText(getActivity().getString(R.string.action_about));
        solo.waitForActivity(AboutActivity.class);
        Assert.assertTrue(solo.searchText("Marcos"));


    }

    public void testNavigationFull() throws Exception{

        solo.assertCurrentActivity("Acitivity errada", HomeActivity.class);
        solo.clickInList(1);
        solo.waitForActivity(ShotDetailsActivity.class);
        solo.goBack();
        solo.sendKey(Solo.MENU);
        solo.clickOnText(getActivity().getString(R.string.action_about));
        solo.waitForActivity(AboutActivity.class);
        solo.goBack();
        Assert.assertTrue(solo.getCurrentActivity().getLocalClassName().equals("activity.HomeActivity"));

    }


    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
}
